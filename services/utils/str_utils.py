from random import choice


def chars_range(char_start, char_end):
    char_start = char_start if type(char_start) is int else ord(char_start)
    char_end = char_start if type(char_end) is int else ord(char_end)

    return list(range(char_start, char_end))


lower_case_chars_range = chars_range('a', 'z')
upper_case_chars_range = chars_range('A', 'Z')


def generate_random_chars(num_chars=1, num_choices=0, joined='', repeat=1):
    choices = []
    for i in range(num_choices):
        random_case = choice([lower_case_chars_range, upper_case_chars_range])
        choices.append(chr(choice(random_case)))

    choices = choices if choices else (lower_case_chars_range + upper_case_chars_range)

    selection = [choice(choices) for i in range(0, num_chars)]
    return (joined.join(selection) if type(joined) is str else selection) * repeat
