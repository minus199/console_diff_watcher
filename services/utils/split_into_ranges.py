def split_into_ranges(nums):
    prev_val = None
    prev_group = []
    groups = {}
    for _h in nums:
        current_val = _h[0][0]
        if prev_val is None:  # first iteration
            prev_group.append(current_val)

        if prev_val is not None and prev_val + 1 == current_val:  # use same group
            prev_group.append(current_val)

        if prev_val is not None and prev_val + 1 < current_val:
            group_name = prev_group[0] if len(prev_group) == 1 else f'{prev_group[0]}:{prev_group[-1]}'
            groups[group_name] = prev_group
            prev_group = [current_val]

        prev_val = current_val

    return groups
