from difflib import ndiff
from services.diff.line import Line


class DiffHandler:
    __slots__ = 'src', 'dest', 'diff', 'diff_lines'
    _mapped = ['!', '*****', '***', '---']

    def __init__(self, src, dest) -> None:
        self.src = src
        self.dest = dest
        self.diff = None
        self.diff_lines = None

    def compute_diff(self):
        self.diff = ndiff([l.decode() for l in self.src], [l.decode() for l in self.dest])
        self.diff_lines = [Line.resolver(i, l).format() for i, l in enumerate(self.diff)]
        return self
