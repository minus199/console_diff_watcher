import os

from shutil import copy2 as copy_file
from watchdog.events import LoggingEventHandler

from services.diff.diff_handler import DiffHandler
from services.ui.diff_view import DiffViewer


class DiffManager(LoggingEventHandler):
    """Logs all the events captured."""

    def __init__(self, left, right):
        self.diff_printer = None

        super(DiffManager, self).__init__()
        self.left = left
        self.right = right

    def _print_diff_files(self, event):
        dst_file = os.path.join(self.right, os.path.basename(event.src_path))
        if not os.path.exists(dst_file):
            copy_file(event.src_path, self.right)

        with open(event.src_path, 'rb') as src_file, open(dst_file, 'rb') as dst_file:
            src_content = src_file.readlines()
            dst_content = dst_file.readlines()
            dh = DiffHandler(src_content, dst_content)
            lines = dh.compute_diff().diff_lines

        x = [l.formatted_diff for l in lines]
        App = DiffViewer(x)
        App.run()

    def on_moved(self, event):
        super(DiffManager, self).on_moved(event)
        self._print_diff_files(event)

    def on_created(self, event):
        super(DiffManager, self).on_created(event)
        self._print_diff_files(event)

    def on_deleted(self, event):
        super(DiffManager, self).on_deleted(event)
        self._print_diff_files(event)

    def on_modified(self, event):
        super(DiffManager, self).on_modified(event)
        self._print_diff_files(event)
