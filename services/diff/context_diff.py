
# self.diff = SequenceMatcher(a=self.src, b=self.dest)
# self.grouped_codes = list(self.diff.get_opcodes())
# self.diff = context_diff(left, right, fromfile='src', tofile='dest')


def versions(self):
    def compute_bounds(line):
        line_parts = line.split(' ')
        if line_parts[0].startswith('*'):
            if len(line_parts) > 3:
                return '*' * 3

            return '-' * 3

        # if line_parts[0].startswith('-')

    acc = {'src_file': '', 'dest_file': '', 'lines': []}
    current = {}

    def bounds(line_num, _line):
        if _line.startswith(DiffHandler._mapped[0]):
            return 'guide'

        if _line.startswith(DiffHandler._mapped[1]):
            return 'start_section'

        if _line.startswith(DiffHandler._mapped[2]):
            return 'src_file' if line_num == 0 else 'section_add'

        if _line.startswith(DiffHandler._mapped[3]):
            return 'dest_file' if line_num == 1 else 'section_delete'

        raise Exception("Unknown symbol ", _line.split(' ')[0])

    acc = {'src_file': '', 'dest_file': '', 'lines': []}
    current = {}
    diff = context_diff(a=[l.decode() for l in self.src], b=[l.decode() for l in self.dest], fromfile='src', tofile='dest')
    prev_bound = ''
    is_last_guide = False
    for i, line in enumerate(diff):
        bound = bounds(i, line)

        if bound == 'start_section':
            prev_bound = bound

        elif bound == 'guide':
            current[prev_bound].append((bound, i, line))
            is_last_guide = True

        elif bound in ['section_add', 'section_delete']:
            if is_last_guide:
                acc['lines'].append(current)
                current = {}

            current[bound] = [(bound, i, line)]
            prev_bound = bound

        elif bound in ['src_file', 'dest_file']:
            acc[bound] = line
            prev_bound = bound


class Line:
    def __init__(self, line_num, line) -> None:
        self._formatter_a = color_codes.formatter(Text.WHITE, Background.BLACK, Style.NO_EFFECT)
        self._formatter_b = color_codes.formatter(Text.WHITE, Background.BLACK, Style.NO_EFFECT)

        self.locked = False
        self.num = line_num
        self.line = line
        self.diff = ()
        self.formatted_diff = ''

    def inject(self, line: 'Line'):
        raise NotImplementedError("Implement inject")

    def lock(self):
        self.locked = True

    def format(self):
        self.formatted_diff = self._formatter_a(self.diff[1]), self._formatter_b(self.diff[2])
        return self

    def _raise_on_lock(self):
        if self.locked:
            raise Exception("section_add is locked")

    @property
    def is_guide(self):
        return False

    @staticmethod
    def resolver(line_num, raw_line):
        raw_line = raw_line.split(" ")
        symbol, line = raw_line[0], ' '.join(raw_line[1:])
        symbol = symbol if len(symbol) < 5 else symbol[:5]

        try:
            return [kls for kls in [SrcFile, DestFile, SectionStart, SectionAdd, SectionDelete, Guide]
                    if kls._criteria(line_num, symbol)][0](line_num, line)
        except (IndexError, KeyError) as e:
            raise Exception("Invalid line", raw_line)


class SrcFile(Line):
    _symbol = '***'
    _bound = 'src_file'
    _criteria = lambda num, symbol: num == 0 and symbol == SrcFile._symbol


class DestFile(Line):
    _symbol = '---'
    _bound = 'dest_file'
    _criteria = lambda num, symbol: num == 1 and symbol == DestFile._symbol


class SectionStart(Line):
    _symbol = '*****'
    _bound = 'section_start'
    _criteria = lambda num, symbol: num > 1 and symbol == SectionStart._symbol

    def __init__(self, line_num, line) -> None:
        super(SectionStart, self).__init__(line_num, line)
        self.section_add: SectionAdd = None
        self.section_del: SectionDelete = None

    def inject(self, line: 'Line'):
        self._raise_on_lock()

        if type(line) not in [SectionAdd, SectionDelete, Guide]:
            raise Exception("Invalid injection", type(line), line)

        if type(line) == SectionAdd:
            self.section_add = line
            return self

        if type(line) == SectionDelete:
            self.section_add.lock()
            self.section_del = line
            return self

        try:
            self.section_add.inject(line)
        except Exception as e:
            self.section_del.inject(line)

        return self


class SectionAdd(Line):
    _symbol = '***'
    _bound = 'section_add'
    _criteria = lambda num, symbol: num > 1 and symbol == SectionAdd._symbol

    def __init__(self, line_num, line) -> None:
        super(SectionAdd, self).__init__(line_num, line)
        self.lines = []

    def inject(self, line: 'Guide'):
        self._raise_on_lock()
        self.lines.append(line)
        return self


class SectionDelete(Line):
    _symbol = '---'
    _bound = 'section_delete'
    _criteria = lambda num, symbol: num > 1 and symbol == SectionDelete._symbol

    def __init__(self, line_num, line) -> None:
        super(SectionDelete, self).__init__(line_num, line)
        self.lines = []

    def inject(self, line: 'Guide'):
        self._raise_on_lock()
        self.lines.append(line)
        return self


class Guide(Line):
    _symbol = '!'
    _bound = 'guide'
    _criteria = lambda num, symbol: num > 1 and symbol == Guide._symbol

    def __init__(self, line_num, line) -> None:
        super(Guide, self).__init__(line_num, line)

    @property
    def is_guide(self):
        return True
