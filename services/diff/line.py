from services.ui.coloring.native_terminal_color_print import formatter
from services.ui.coloring.urwid_colors_driver import Background as bg, Foreground as fg


class Line:
    def __init__(self, line_num, line) -> None:
        # self._formatter = color_codes.formatter(Text.WHITE, Background.BLACK, Style.NO_EFFECT)

        self.locked = False
        self.num = line_num
        self.line = line
        self.diff = ()
        self.formatted_diff = ''

    def inject(self, line: 'Line'):
        raise NotImplementedError("Implement inject")

    def lock(self):
        self.locked = True

    def format(self, do_print=False):
        self.formatted_diff = self._formatter(self.line)
        if do_print:
            print(self.formatted_diff)

        return self

    def _raise_on_lock(self):
        if self.locked:
            raise Exception("section_add is locked")

    @property
    def is_guide(self):
        return False

    @staticmethod
    def resolver(line_num, raw_line):
        symbol, line = raw_line[0], raw_line[2:]  # remove the space after prefix
        symbol = symbol if len(symbol) < 5 else symbol[:5]

        try:
            resolved_kls = [kls for kls in [DeleteLine, InsertLine, EqualLine, NotPresentLine] if kls._symbol == symbol][0]
            return resolved_kls(line_num, line)
        except (IndexError, KeyError) as e:
            raise Exception("Invalid line", raw_line, symbol, line)
        except Exception as e:
            raise Exception("Unable to resolve", raw_line, symbol, line)


class DeleteLine(Line):
    _symbol = '-'
    _bound = 'removed'

    # 'delete' 	a[i1:i2] should be deleted. Note that j1 == j2 in this case.
    def __init__(self, line_num, grouped_codes) -> None:
        super(DeleteLine, self).__init__(line_num, grouped_codes)
        self._formatter = color_codes.formatter(Text.RED, Background.BLACK, Style.ITALIC, '-')


class InsertLine(Line):
    _symbol = '+'
    _bound = 'inserted'

    # 'insert' 	b[j1:j2] should be inserted at a[i1:i1]. Note that i1 == i2 in this case.
    def __init__(self, line_num, grouped_codes) -> None:
        super(InsertLine, self).__init__(line_num, grouped_codes)
        self._formatter = color_codes.formatter(Text.GREEN, Background.BLACK, Style.ITALIC, '+')


class EqualLine(Line):
    _symbol = ' '
    _bound = 'commons'

    # 'equal' 	a[i1:i2] == b[j1:j2] (the sub-sequences are equal).
    def __init__(self, line_num, grouped_codes) -> None:
        super(EqualLine, self).__init__(line_num, grouped_codes)
        self._formatter = color_codes.formatter(Text.BLUE, Background.BLACK, Style.NO_EFFECT)


class NotPresentLine(Line):
    _symbol = '?'
    _bound = 'n/a'

    def __init__(self, line_num, line) -> None:
        super().__init__(line_num, line)
        self._formatter = color_codes.formatter(Text.PURPLE, Background.WHITE, Style.NO_EFFECT)
