import time
import logging
from watchdog.observers import Observer
import os

from diff.diff_manager import DiffManager

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

    # path = './testing'  # sys.argv[1] if len(sys.argv) > 1 else '.'
    base_dir = os.path.abspath('./app/testing')
    watch_dir = os.path.join(base_dir, 'src')
    dst = os.path.join(base_dir, 'target')
    if not os.path.exists(dst):
        os.makedirs(dst)

    event_handler = DiffManager(watch_dir, dst)

    observer = Observer()
    observer.schedule(event_handler, watch_dir, recursive=True)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()

    observer.join()
