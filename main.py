from functools import partial

from urwid import raw_display, ExitMainLoop, MainLoop, ListBox, Columns, AttrWrap, Text, Frame, Edit, connect_signal

from services.utils import str_utils
from tui.coloring.diff_palette import palette
from tui.diff_view import DiffView
from tui.highlighted_line import DiffedLine


def unhandled_input(key):
    if key in ('Q', 'q', 'esc'):
        raise ExitMainLoop()


def line_fcs_handler(widget, newtext):
    widget.set_text("Edit widget changed to %s" % newtext)


def boot_ui():
    screen = raw_display.Screen()
    screen.register_palette(palette)

    header = AttrWrap(Edit("Hey oh"), 'head')
    footer = AttrWrap(Text("Waddup"), 'foot')

    # edit = Edit('')

    diff_view = ListBox(DiffView([DiffedLine(str_utils.generate_random_chars(5, 3, repeat=10)) for x in range(0, 100)]))
    # key = connect_signal(diff_view, 'change', partial(line_fcs_handler, header))
    diff_container = Frame(AttrWrap(diff_view, 'body'), header=header, footer=footer)

    # x = Columns([diff_view, diff_view], dividechars=3, focus_column=0, box_columns=[0, 1])

    MainLoop(diff_container, screen=screen, unhandled_input=unhandled_input).run()


if __name__ == '__main__':
    boot_ui()
