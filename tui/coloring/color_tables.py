codes_raw = '#00f#06f#08f#0af#0df#0ff#60f#00d#06d#08d#0ad#0dd#0fd#80f#60d#00a#06a#08a#0aa#0da#0fa#a0f#80d#60a' \
            '#008#068#088#0a8#0d8#0f8#d0f#a0d#80d#608#006#066#086#0a6#0d6#0f6#f0f#d0d#a0a#808#606#000#060#080#0a0' \
            '#0d0#0f0#0f6#0f8#0fa#0fd#0ff#f0d#d0a#a08#806#600#660#680#6a0#6d0#6f0#6f6#6f8#6fa#6fd#6ff#0df#f0a#d08' \
            '#a06#800#860#880#8a0#8d0#8f0#8f6#8f8#8fa#8fd#8ff#6df#0af#f08#d06#a00#a60#a80#aa0#ad0#af0#af6#af8#afa' \
            '#afd#aff#8df#6af#08f#f06#d00#d60#d80#da0#dd0#df0#df6#df8#dfa#dfd#dff#adf#8af#68f#06f#f00#f60#f80#fa0' \
            '#fd0#ff0#ff6#ff8#ffa#ffd#fff#ddf#aaf#88f#66f#00f#fd0#fd6#fd8#fda#fdd#fdf#daf#a8f#86f#60f#66d#68d#6ad#6dd' \
            '#fa0#fa6#fa8#faa#fad#faf#d8f#a6f#80f#86d#66a#68a#6aa#6da#f80#f86#f88#f8a#f8d#f8f#d6f#a0f#a6d#86a#668#688' \
            '#6a8#6d8#f60#f66#f68#f6a#f6d#f6f#d0f#d6d#a6a#868#666#686#6a6#6d6#6d8#6da#6dd#f00#f06#f08#f0a#f0d#f0f#d6a' \
            '#a68#866#886#8a6#8d6#8d8#8da#8dd#6ad#d68#a66#a86#aa6#ad6#ad8#ada#add#8ad#68d#d66#d86#da6#dd6#dd8#dda#ddd' \
            '#aad#88d#66d#da6#da8#daa#dad#a8d#86d#88a#8aa#d86#d88#d8a#d8d#a6d#a8a#888#8a8#8aa#d66#d68#d6a#d6d#a88#aa8' \
            '#aaa#88a#a88#a8a'.split("#")

chart_16 = ['dark_red_', 'dark_magenta_', 'dark_blue_', 'dark_cyan_', 'dark_green_yellow_', 'dark_gray___',
            'light_red', 'light_magenta', 'light_blue', 'light_cyan', 'light_greenblack_______', 'light_gray__',
            'brown__', 'white_______']

chart_88 = ['#00f', '#08f', '#0cf', '#0ff', '#80f', '#00c', '#08c', '#0cc', '#0fc', '#c0f', '#80c', '#008', '#088', '#0c8',
            '#0f8', '#f0f', '#c0c', '#808', '#000', '#080', '#0c0', '#0f0', '#0f8', '#0fc', '#0ff', '#88c', '#8cc', '#f0c',
            '#c08', '#800', '#880', '#8c0', '#8f0', '#8f8', '#8fc', '#8ff', '#0cf', '#c8c', '#888', '#8c8', '#8cc', '#f08',
            '#c00', '#c80', '#cc0', '#cf0', '#cf8', '#cfc', '#cff', '#8cf', '#08f', '#c88', '#cc8', '#ccc', '#88c', '#f00',
            '#f80', '#fc0', '#ff0', '#ff8', '#ffc', '#fff', '#ccf', '#88f', '#00f', '#c88', '#c8c', '#fc0', '#fc8', '#fcc',
            '#fcf', '#c8f', '#80f', '#f80', '#f88', '#f8c', '#f8f', '#c0f', '#f00', '#f08', '#f0c', '#f0f']

chart_256 = [8000, 0, 6, 8, 10, 13, 15, 15, 96, 102, 104, 106, 109, 111, 111, 128, 134, 136, 138,
             141, 143, 143, 160, 166, 168, 170, 173, 175, 175, 208, 214, 216, 218, 221, 223,
             223, 240, 246, 246, 248, 248, 250, 250, 253, 253, 255, 255, 1536, 1542, 1544,
             1546, 1549, 1551, 1551, 1632, 1638, 1640, 1642, 1645, 1645, 1647, 1664, 1670,
             1672, 1674, 1677, 1677, 1679, 1696, 1702, 1704, 1706, 1709,
             1709, 1711, 1744, 1750, 1752, 1752, 1754, 1754, 1757, 1757, 1759, 1776,
             1782, 1784, 1786, 1789, 1791, 2048, 2054, 2056, 2061, 2061, 2063, 2063,
             2144, 2150, 2152, 2154, 2157, 2157, 2159, 2176, 2182, 2184, 2186, 2186,
             2189, 2191, 2208, 2214, 2216, 2218, 2218, 2221, 2223, 2256, 2262, 2264,
             2266, 2269, 2271, 2288, 2294, 2296, 2298, 2301, 2303, 2560, 2566, 2568, 2570,
             2573, 2575, 2575, 2656, 2662, 2664, 2666, 2669, 2669, 2671, 2688, 2694, 2696,
             2696, 2698, 2698, 2701, 2703, 2720, 2726, 2728, 2730, 2733, 2735, 2768, 2774,
             2776, 2778, 2781, 2783, 2800, 2806, 2808, 2810, 2813, 2815, 3328, 3334, 3336,
             3338, 3341, 3343, 3343, 3424, 3430, 3430, 3432, 3432, 3434, 3434, 3437, 3437,
             3439, 3456, 3462, 3462, 3464, 3466, 3469, 3471, 3488, 3494, 3494, 3496, 3498,
             3501, 3503, 3536, 3542, 3544, 3546, 3549, 3551, 3568, 3574, 3576, 3578, 3581, 3583,
             3840, 3840, 3846, 3846, 3848, 3848, 3850, 3850, 3853, 3853, 3855, 3855, 3936, 3936,
             3942, 3944, 3946, 3949, 3951, 3968, 3968, 3974, 3976, 3978, 3981, 3983, 4000, 4000,
             4006, 4008, 4010, 4013, 4015, 4048, 4048, 4054, 4056, 4058, 4061, 4063, 4080, 4086,
             4088, 4090, 4093, 4095]
