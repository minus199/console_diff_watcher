from collections import namedtuple
from enum import Enum
from functools import reduce

CompiledPalette = namedtuple('CompiledPalette', 'label,foreground,background,mono,foreground_high,background_high')


class Connector:
    __slots__ = ['label', 'a', 'b', 'descriptor_fg', 'descriptor_bg']

    def __init__(self, a, b) -> None:
        super().__init__()

        self.label = None
        self.descriptor_fg: Foreground.Descriptor = None
        self.descriptor_bg: Background.Descriptor = None

        self.a = a
        self.b = b

    def apply(self):
        if type(self.a) is Connector:
            self.descriptor_fg = self.a.descriptor_fg
            self.descriptor_bg = self.a.descriptor_bg
            self.b.inject_descriptor(self)
            return self

        self.a.inject_descriptor(self)
        self.b.inject_descriptor(self)

        return self

    def compile(self, label):
        fg, fg_h = self.descriptor_fg.compile()
        bg, bg_h = self.descriptor_bg.compile()
        return CompiledPalette(label, fg, bg, 'standout,underline', fg_h, bg_h)

    def __and__(self, other):
        return Connector(self, other).apply()

    class Accumaltor(list):
        def __add__(self, other):
            try:
                self.extend(other)
            except Exception as e:
                self.append(other)
            return self

        def join(self):
            return ','.join([x.value for x in self])

    class Mixin:
        @property
        def parent(self):
            raise NotImplementedError()

        def inject_descriptor(self, target):
            descriptor_name = self.parent.Descriptor.full_name
            descriptor = getattr(target, descriptor_name) or self.parent.Descriptor()
            setattr(target, descriptor_name, descriptor)
            descriptor.inject(self)

        def __str__(self):
            return self.value

        def __mod__(self, other):
            if type(other) is str:
                return f'{self.value},{other}' if other else self.value

            return (other + self).join()

        def __add__(self, other):
            summed = other if type(other) is Connector.Accumaltor else Connector.Accumaltor()
            return summed + other + self

        def __and__(self, other):
            return Connector(self, other).apply()

    class FgMixin(Mixin):
        @property
        def parent(self):
            return Foreground

    class BgMixin(Mixin):
        @property
        def parent(self):
            return Background


class Foreground:
    class Descriptor:
        full_name = 'descriptor_fg'

        def __init__(self) -> None:
            self.color = set()
            self.decor = set()
            self.high = set()
            self.highdecor = set()
            # self.mono =

        def inject(self, *properties):
            [getattr(self, p.__class__.__name__.lower()).add(p) for p in properties]
            return self

        def compile(self):
            color = self.color.pop() if self.color else Foreground.Color.DEFAULT
            try:
                decor = reduce(lambda acc, d: d + acc, self.decor)
            except TypeError as e:
                decor = ''

            high = (self.high.pop() if self.high else Foreground.High.DEFAULT)
            h_decor = ','.join([x.value for x in self.highdecor])
            high % h_decor
            return color % decor, high % h_decor

        def __and__(self, other: 'Foreground.Descriptor'):
            self.color.update(other.color)
            self.decor.update(other.decor)
            self.high.update(other.high)
            self.highdecor.update(other.highdecor)
            return self

    class Color(Connector.FgMixin, Enum):
        DEFAULT = "default"
        BLACK = "black"
        DARK_RED = "dark red"
        DARK_GREEN = "dark green"
        BROWN = "brown"
        DARK_BLUE = "dark blue"
        DARK_MAGENTA = "dark magenta"
        DARK_CYAN = "dark cyan"
        LIGHT_GRAY = "light gray"
        DARK_GRAY = "dark gray"
        LIGHT_RED = "light red"
        LIGHT_GREEN = "light green"
        YELLOW = "yellow"
        LIGHT_BLUE = "light blue"
        LIGHT_MAGENTA = "light magenta"
        LIGHT_CYAN = "light cyan"
        WHITE = "white"

    class Decor(Connector.FgMixin, Enum):
        D = DEFAULT = "default"
        B = BOLD = 'bold'
        U = UNDERLINE = 'underline'
        BL = BLINK = 'blink'
        SO = STANDOUT = 'standout'
        S = STRIKETHROUGH = 'strikethrough'

    class High(Connector.FgMixin, Enum):
        DEFAULT = "default"
        BLACK = "black"
        DARK_RED = "dark red"
        DARK_GREEN = "dark green"
        BROWN = "brown"
        DARK_BLUE = "dark blue"
        DARK_MAGENTA = "dark magenta"
        DARK_CYAN = "dark cyan"
        LIGHT_GRAY = "light gray"
        DARK_GRAY = "dark gray"
        LIGHT_RED = "light red"
        LIGHT_GREEN = "light green"
        YELLOW = "yellow"
        LIGHT_BLUE = "light blue"
        LIGHT_MAGENTA = "light magenta"
        LIGHT_CYAN = "light cyan"
        WHITE = "white"

    class HighDecor(Connector.FgMixin, Enum):
        D = DEFAULT = "default"
        B = BOLD = 'bold'
        U = UNDERLINE = 'underline'
        BL = BLINK = 'blink'
        SO = STANDOUT = 'standout'
        S = STRIKETHROUGH = 'strikethrough'


class Background:
    class Descriptor:
        full_name = 'descriptor_bg'

        def __init__(self) -> None:
            self.color = set()
            self.high = set()

        def inject(self, *properties):
            [getattr(self, p.__class__.__name__.lower()).add(p) for p in properties]
            return self

        def compile(self):
            namedtuple('fg_pallete', 'fg, fg_high')
            color = (self.color.pop() if self.color else Background.Color.DEFAULT).value
            high = (self.high.pop() if self.high else Background.High.DEFAULT).value
            return f'{color}', f'{high}'

        def __and__(self, other: 'Background.Descriptor'):
            self.color.update(other.color)
            self.high.update(other.high)
            return self

    class Color(Connector.BgMixin, Enum):
        DEFAULT = 'default'
        BLACK = 'black'
        DARK_RED = 'dark red'
        DARK_GREEN = 'dark green'
        BROWN = 'brown'
        DARK_BLUE = 'dark blue'
        DARK_MAGENTA = 'dark magenta'
        DARK_CYAN = 'dark cyan'
        LIGHT_GRAY = 'light gray'

    class High(Connector.BgMixin, Enum):
        DEFAULT = 'default'
        BLACK = 'black'
        DARK_RED = 'dark red'
        DARK_GREEN = 'dark green'
        BROWN = 'brown'
        DARK_BLUE = 'dark blue'
        DARK_MAGENTA = 'dark magenta'
        DARK_CYAN = 'dark cyan'
        LIGHT_GRAY = 'light gray'
