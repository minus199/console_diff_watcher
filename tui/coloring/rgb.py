from collections import namedtuple

from tui.coloring.color_tables import codes_raw

Color = namedtuple('RGB', 'red, green, blue')


class RGB:
    def __init__(self, decimal_color_code) -> None:
        super().__init__()
        self.decimal_color_code = decimal_color_code
        self.as_hex = '{:06x}'.format(decimal_color_code)
        self.as_hex_short = '{:03x}'.format(decimal_color_code)

        r, g, b = [int(f'{"".join(c)}', 16) for c in zip(self.as_hex[::2], self.as_hex[1::2])]
        self.color = Color(r, g, b)

    @staticmethod
    def factory(color_code: int):
        return RGB(color_code)

    @staticmethod
    def numeric_to_hex(color_codes):
        return [f'#{RGB.factory(color_code)}' for color_code in color_codes]

    @staticmethod
    def hexes_table():
        codes = sorted([(int(i, 16), i) for i in codes_raw if i])
        return codes

    def __repr__(self):
        return f'#{self.as_hex_short}, {self.color}'

    def __str__(self) -> str:
        return self.as_hex_short
