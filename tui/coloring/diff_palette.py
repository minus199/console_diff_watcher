from tui.coloring.urwid_colors_driver import Background as bg, Foreground as fg

"""
High-color example values: 
‘#009’ (0% red, 0% green, 60% red, like HTML colors) 
‘#fcc’ (100% red, 80% green, 80% blue) 
‘g40’ (40% gray, decimal), 
‘g#cc’ (80% gray, hex), 
‘#000’, ‘g0’, ‘g#00’ (black), 
‘#fff’, ‘g100’, ‘g#ff’ (white) 
‘h8’ (color number 8), 
‘h255’ (color number 255)
"""
_palette = [
    # fg                #bg            mono                fg_h             bg_h
    ('header', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
    ('panel', 'light gray', 'dark blue', '', '#ffd', '#00a'),
    ('focus', 'light gray', 'dark cyan', 'standout', '#ff8', '#806'),

    # name, foreground, background, mono=None, foreground_high=None, background_high=None
    ('header', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
    ('line_added', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
    ('line_removed', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
    ('line_eq', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
    ('line_unknown', 'black,underline', 'light gray', 'standout,underline', 'black,underline', '#88a'),
    ('panel', 'light gray', 'dark blue', '', '#ffd', '#00a'),
    ('focus', 'light gray', 'dark cyan', 'standout', '#ff8', '#806'),
]

palette = [
    (fg.Color.LIGHT_GREEN & fg.Decor.B & bg.Color.DEFAULT).compile('line_add'),
    (fg.Color.DARK_RED & fg.Decor.B & bg.Color.DEFAULT).compile('line_del'),
    (fg.Color.LIGHT_BLUE & fg.Decor.B & bg.Color.DEFAULT).compile('line_eq'),
    (fg.Color.DARK_MAGENTA & fg.Decor.B & bg.Color.DEFAULT).compile('line_na'),
    (fg.Color.BLACK & bg.Color.LIGHT_GRAY).compile('line_focus_out')
]
