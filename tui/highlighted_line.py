from random import choice

from urwid import Widget, TextCanvas

styles = ['line_add', 'line_del', 'line_eq', 'line_na']


class DiffedLine(Widget):
    _sizing = frozenset(['flow'])
    _selectable = True

    def __init__(self, content=''):
        self.raw_content = content
        self.cursor_y = 0
        self.cursor_x = 0

        self.focus_style = choice(styles)
        self.focus_out_style = 'line_focus_out'

    def rows(self, size, focus=False):
        return 1

    def render(self, size, focus=False):
        content = [(self.raw_content.upper() if focus else self.raw_content.lower()).encode('utf-8')]
        # attrs = rle_encoding.encode(','.join(_plt[int(focus)][:1]).encode())
        attrs = [[("line_focus_" + ('in' if focus else 'out'), len(self.raw_content))]]

        attrs = [
            [(
                self.focus_style if not focus else self.focus_out_style,
                len(self.raw_content)
            )]
        ]
        cursor = self.get_cursor_coords(size) if focus else None
        return TextCanvas(content, attr=attrs, cursor=cursor, maxcol=size[0])

    def get_cursor_coords(self, size):
        return self.cursor_x, min(self.cursor_y, size[0] - 1)

    def keypress(self, size, key):
        if key == 'left':
            self.cursor_x = max(0, self.cursor_x - 1)
        elif key == 'right':
            self.cursor_x = min(self.cursor_x + 1, len(self.raw_content) - 1)
        else:
            return key

        self._invalidate()

    def __str__(self) -> str:
        return self.raw_content[:15] + '...'
