from urwid import AttrMap, SimpleListWalker, ExitMainLoop, Divider, Button


class DiffView(SimpleListWalker):
    @staticmethod
    def click_exit(button):
        raise ExitMainLoop()

    @staticmethod
    def fcs(widget):
        # wrap widgets that can take focus
        return AttrMap(widget, None, 'focus')

    def __init__(self, contents):
        super().__init__(contents)
        self.extend([Divider(), DiffView.fcs(Button("Exit", DiffView.click_exit))])
